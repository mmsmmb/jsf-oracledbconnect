package beans;

import model.Member;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Macost on 05.12.2015.
 */

@ManagedBean//(name = "memberBean")
@SessionScoped //RequestScoped
public class MemberBean{
    private int cid;
    private int nid;
    private int g = 0;
    private List<Member> list = null;

    private String name;
    private String surname;
    private int rate;
    private int wtime;

    public int getCid() {
        return cid;
    }

    public void setCid(int id) {
        this.cid = id;
    }

    public int getWtime() {
        return wtime;
    }

    public void setWtime(int wtime) {
        this.wtime = wtime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public List<Member> getMemberList() {
        //List<Member> list = null;
        try {
            Connection con = getConnection();
            list = new ArrayList<Member>();
            PreparedStatement pst = null;
            ResultSet rs = null;
            try {
                String sql = "select a.*, b.WTIME, b.SALARY from aemp a join asal b on a.id = b.cid order by id ";
                //String sql = "select * from aemp";
                //String sql = "select * from asal";
                pst = con.prepareStatement(sql);
                rs = pst.executeQuery();
                while (rs.next()) {
                    Member member = new Member();
                    //����������
                    member.setId(rs.getInt("id"));
                    member.setName(rs.getString("name"));
                    member.setSurname(rs.getString("surname"));
                    member.setRate(rs.getInt("rate"));
//                    member.setCid(rs.getInt(1));
//                    member.setMid(rs.getInt(2));
//                    member.setCurrent_month(rs.getString(3));
                    member.setWtime(rs.getInt("wtime"));
                    member.setSalary(rs.getInt("salary"));

                    list.add(member);
                }

            } finally {
                rs.close();
                pst.close();
                con.close();
                System.out.println("con.close -----");


            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("SQL Exception..." + e.getMessage());  //e.getMessage()
        }
        return list;
    }

    public String addAction(){
        try(Connection con = getConnection()){
            int nextId;
            try(PreparedStatement pst = con.prepareStatement("SELECT max(id) FROM aemp")){
                ResultSet rs = pst.executeQuery();
                rs.next();
                nextId = rs.getInt(1);
            }// stmt is auto closed here, even if SQLException is thrown

            nextId = nextId + 1;
            try(PreparedStatement pst = con.prepareStatement("INSERT INTO aemp(id, name, surname, rate) VALUES(?,?,?,?)")){
                pst.setInt(1, nextId);
                pst.setString(2, name);
                pst.setString(3, surname);
                pst.setInt(4, rate);
                pst.executeUpdate();
                System.out.println("Data Added AEMP");
            }// stmt is auto closed here, even if SQLException is thrown

            try(PreparedStatement pst = con.prepareStatement("INSERT INTO asal(cid, mid, current_month, wtime, salary) VALUES(?,?,?,?,?)")){
                pst.setInt(1, nextId);
                pst.setInt(2, nextId);
                pst.setString(3, "������");
                pst.setInt(4, 0);
                pst.setInt(5, 0);
                pst.executeUpdate();
                System.out.println("Data Added ASAL");
            }// stmt is auto closed here, even if SQLException is thrown

        }// connection is auto closed here, even if SQLException is thrown
        catch (SQLException e){
            e.printStackTrace();
            //System.out.println("SQL Exception..." + e.getMessage());  //e.getMessage()
        }

        return null;
    }

    public String deleteAction(Member member){
        int curId = member.getId();
        list.remove(member);
        System.out.println("deleteAction() used");
        try(Connection con = getConnection()){
            try(PreparedStatement pst = con.prepareStatement("DELETE FROM asal WHERE cid = ?")){
                pst.setInt(1, curId);
                pst.executeUpdate();
                System.out.println("del row from asal");
            }

            try(PreparedStatement pst = con.prepareStatement("DELETE FROM aemp WHERE id = ?")) {
                pst.setInt(1, curId);
                pst.executeUpdate();
                System.out.println("del row from aemp");
            }
        }// connection is auto closed here, even if SQLException is thrown
        catch (SQLException e){
            e.printStackTrace();
    }
        return null;
    }


    public String timeAdd(){
        try(Connection con = getConnection()){
            int curRate;
            try(PreparedStatement pst = con.prepareStatement("UPDATE asal SET wtime = ? WHERE cid = ? ")){
                pst.setInt(1, wtime);
                pst.setInt(2, cid);
                pst.executeUpdate();
                System.out.println("Update wtime");
            }
            try(PreparedStatement pst = con.prepareStatement("SELECT rate FROM aemp WHERE id = ?")){
                pst.setInt(1, cid);
                ResultSet rs = pst.executeQuery();
                rs.next();
                curRate = rs.getInt(1);

            }
            try(PreparedStatement pst = con.prepareStatement("UPDATE asal SET salary = ? WHERE cid = ?")){
                int salary = wtime * curRate;
                pst.setInt(1, salary);
                pst.setInt(2, cid);
                pst.executeUpdate();
            }

        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private Connection getConnection() {
        Connection con = null;
        try {
            //this is locale DB
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String url = "jdbc:oracle:thin:@localhost:1521:XE"; //jdbc:oracle:thin:@hostname:port:databaseName
            String user = "SYSTEM";
            String password = "5534621";
//            Login settings UNC database
//            String url = "jdbc:oracle:thin:@edu-netcracker.com:1520:XE";
//            String user = "unc15_mama";
//            String password = "unc15_mama";
            con = DriverManager.getConnection(url, user, password);
            System.out.println("Connection completed");
        } catch (ClassNotFoundException e) {
            System.out.println("Invalid Driver..." + e.getMessage());
        } catch (SQLException e) {
            System.out.println("Connection failed..." + e.getMessage());
        }
        return con;
    }

}
